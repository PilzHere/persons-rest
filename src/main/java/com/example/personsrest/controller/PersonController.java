package com.example.personsrest.controller;

import com.example.personsrest.domain.UpdatePerson;
import com.example.personsrest.domain.CreatePerson;
import com.example.personsrest.domain.Person;
import com.example.personsrest.domain.PersonDTO;
import com.example.personsrest.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author pilzhere
 * @created 13/01/2022 - 10:57 PM
 * @project persons-rest
 */

@RestController
@AllArgsConstructor
@RequestMapping ("/api/persons")
public class PersonController {

    PersonService personService;

    @GetMapping
    public List<PersonDTO> getAllPerson (@RequestParam (required = false) Map<String, String> searchParams) {
        if (searchParams.isEmpty()) {
            return personService.getAllPerson().stream().map(this::toDTO).collect(Collectors.toList());
        } else {
            return personService.find(searchParams).stream().map(this::toDTO).collect(Collectors.toList());
        }
    }

    @PostMapping
    public PersonDTO savePerson (@RequestBody CreatePerson createPerson) {
        return toDTO(personService.savePerson(createPerson));
    }

    @GetMapping ("/{id}")
    public PersonDTO findById (@PathVariable ("id") String id) {
        return toDTO(personService.findById(id));
    }

    @PutMapping ("/{id}")
    public PersonDTO updatePerson (@PathVariable ("id") String id, @RequestBody UpdatePerson updatePerson) {
        return toDTO(personService.updatePerson(id, updatePerson.getName(), updatePerson.getCity(), updatePerson.getAge()));
    }

    @DeleteMapping ("/{id}")
    public PersonDTO deletePerson (@PathVariable ("id") String id) {
        return toDTO(personService.deletePerson(id));
    }

    @PutMapping ("/{id}/addGroup/{groupname}")
    public PersonDTO addGroup (@PathVariable ("id") String id, @PathVariable ("groupname") String groupName) {
        return toDTO(personService.addGroup(id, groupName));
    }

    @DeleteMapping ("/{id}/removeGroup/{groupname}")
    public PersonDTO removeGroup (@PathVariable ("id") String id, @PathVariable ("groupname") String groupName) {
        return toDTO(personService.removeGroup(id, groupName));
    }

    public PersonDTO toDTO (Person person) {
        return new PersonDTO(
                person.getId(),
                person.getName(),
                person.getAge(),
                person.getCity(),
                person.getGroups().stream()
                        .map(groupId -> personService.getGroupNameById(groupId))
                        .collect(Collectors.toList())
        );
    }
}
