package com.example.personsrest.remote;

import com.example.personsrest.KeyCloakToken;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.Objects;

/**
 * @author pilzhere
 * @created 22/01/2022 - 12:41 PM
 * @project persons-rest
 */

public class GroupRemoteImpl implements GroupRemote {
    WebClient webClient = WebClient.create("https://groups.edu.sensera.se/");
    final int timeOut = 30;
    KeyCloakToken keyCloakToken = KeyCloakToken.acquire("https://iam.sensera.se/", "test",
            "group-api", "user", "djnJnPf7VCQvp3Fc").block(Duration.ofSeconds(timeOut));

    @Override
    public String getNameById (String groupId) {
        return Objects.requireNonNull(webClient
                        .get()
                        .uri("api/groups/" + groupId)
                        .header("Authorization", "Bearer " + keyCloakToken.getAccessToken())
                        .retrieve()
                        .bodyToMono(Group.class)
                        .block())
                .getName();
    }

    @Override
    public String createGroup (String name) {
        return Objects.requireNonNull(webClient
                        .post()
                        .uri("api/groups/")
                        .header("Authorization", "Bearer " + keyCloakToken.getAccessToken())
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .body(BodyInserters.fromValue(new CreateGroup(name)))
                        .retrieve()
                        .bodyToMono(Group.class)
                        .block())
                .getId();
    }

    @Override
    public String removeGroup (String name) {
        return Objects.requireNonNull(webClient
                        .delete()
                        .uri("api/groups/" + name)
                        .header("Authorization", "Bearer " + keyCloakToken.getAccessToken())
                        .retrieve()
                        .bodyToMono(Group.class)
                        .block())
                .getName();
    }
}
