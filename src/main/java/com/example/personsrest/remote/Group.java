package com.example.personsrest.remote;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

/**
 * @author pilzhere
 * @created 22/01/2022 - 4:35 PM
 * @project persons-rest
 */

@Value
public class Group {
    String id;
    String name;

    @JsonCreator
    public Group (@JsonProperty ("id") String id, @JsonProperty ("name") String name) {
        this.id = id;
        this.name = name;
    }
}
