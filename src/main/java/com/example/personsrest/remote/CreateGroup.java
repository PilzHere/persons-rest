package com.example.personsrest.remote;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

/**
 * @author pilzhere
 * @created 22/01/2022 - 4:32 PM
 * @project persons-rest
 */

@Value
public class CreateGroup {
    String name;

    @JsonCreator
    public CreateGroup (@JsonProperty ("name") String name) {
        this.name = name;
    }
}
