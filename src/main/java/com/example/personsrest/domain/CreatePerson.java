package com.example.personsrest.domain;

import lombok.Value;

/**
 * @author pilzhere
 * @created 14/01/2022 - 2:45 PM
 * @project persons-rest
 */

@Value
public class CreatePerson {
    String name;
    int age;
    String city;
}

