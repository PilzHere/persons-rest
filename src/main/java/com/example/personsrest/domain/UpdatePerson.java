package com.example.personsrest.domain;

import lombok.Value;

/**
 * @author pilzhere
 * @created 14/01/2022 - 3:02 PM
 * @project persons-rest
 */
@Value
public class UpdatePerson {
    String name;
    int age;
    String city;
}
