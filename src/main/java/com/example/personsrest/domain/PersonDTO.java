package com.example.personsrest.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

/**
 * @author pilzhere
 * @created 22/01/2022 - 1:47 PM
 * @project persons-rest
 */

@Value
public class PersonDTO {
    String id;
    String name;
    int age;
    String city;
    List<String> groups;

    @JsonCreator
    public PersonDTO (@JsonProperty ("id") String id,
                      @JsonProperty ("name") String name,
                      @JsonProperty ("age") int age,
                      @JsonProperty ("city") String city,
                      @JsonProperty ("groups") List<String> groups) {

        this.id = id;
        this.name = name;
        this.age = age;
        this.city = city;
        this.groups = groups;
    }
}
